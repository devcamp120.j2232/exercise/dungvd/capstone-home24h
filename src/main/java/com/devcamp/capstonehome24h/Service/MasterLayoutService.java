package com.devcamp.capstonehome24h.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.MasterLayout;
import com.devcamp.capstonehome24h.Entity.Project;
import com.devcamp.capstonehome24h.Repository.MasterLayoutRepository;
import com.devcamp.capstonehome24h.Repository.ProjectRepository;

@Service
public class MasterLayoutService {
    @Autowired
    MasterLayoutRepository iMasterLayout;

    @Autowired
    ProjectRepository iProject;
    // Lấy ra toàn bộ master layout
    public List<MasterLayout> getAll() {
        return iMasterLayout.findAll();
    }
    
    // create master layout
    public MasterLayout createMasterLayout(MasterLayout pMasterLayout, int projectId) {
        Project vProject = iProject.findById(projectId).get();
        pMasterLayout.setProject(vProject);
        MasterLayout masterLayoutCreated = iMasterLayout.save(pMasterLayout);
        return masterLayoutCreated;
    }

    // update master layout
    public MasterLayout updateMasterLayout(MasterLayout pMasterLayout, int id, int projectId) {
        Project vProject = iProject.findById(projectId).get();
        MasterLayout vMasterLayout = iMasterLayout.findById(id).get();
        Date vDate = new Date();
        vMasterLayout.setAcreage(pMasterLayout.getAcreage());
        vMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
        vMasterLayout.setDateUpdate(vDate);
        vMasterLayout.setDescription(pMasterLayout.getDescription());
        vMasterLayout.setName(pMasterLayout.getName());
        vMasterLayout.setPhoto(pMasterLayout.getPhoto());
        vMasterLayout.setProject(vProject);
        MasterLayout masterLayoutUpdated = iMasterLayout.save(vMasterLayout);
        return masterLayoutUpdated;
    }

    // delete by id
    public void deleteMasterLayout(int id) {
        iMasterLayout.deleteById(id);
    }
}
