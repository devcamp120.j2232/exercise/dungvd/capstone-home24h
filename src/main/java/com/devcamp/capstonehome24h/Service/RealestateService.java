package com.devcamp.capstonehome24h.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Realestate;
import com.devcamp.capstonehome24h.Repository.RealestateRepository;

@Service
public class RealestateService {
    @Autowired
    RealestateRepository realEstateRepository;

    public List<Realestate> getAll() {
        return realEstateRepository.findAll();
    }

    // Create a new real estate
    public Realestate createRealEstate(Realestate realEstate) {
        return realEstateRepository.save(realEstate);
    }

    // Update a real estate
    public Realestate updateRealEstate(Realestate realEstate, int id) {
        Realestate existingRealEstate = realEstateRepository.findById(id).get();
        Date newDate = new Date();
        existingRealEstate.setTitle(realEstate.getTitle());
        existingRealEstate.setType(realEstate.getType());
        existingRealEstate.setRequest(realEstate.getRequest());
        existingRealEstate.setProvinceId(realEstate.getProvinceId());
        existingRealEstate.setDistrictId(realEstate.getDistrictId());
        existingRealEstate.setWardsId(realEstate.getWardsId());
        existingRealEstate.setStreetId(realEstate.getStreetId());
        existingRealEstate.setProjectId(realEstate.getProjectId());
        existingRealEstate.setAddress(realEstate.getAddress());
        existingRealEstate.setCustomerId(realEstate.getCustomerId());
        existingRealEstate.setPrice(realEstate.getPrice());
        existingRealEstate.setPriceMin(realEstate.getPriceMin());
        existingRealEstate.setPriceTime(realEstate.getPriceTime());
        existingRealEstate.setDateCreate(newDate);
        existingRealEstate.setAcreage(realEstate.getAcreage());
        existingRealEstate.setDirection(realEstate.getDirection());
        existingRealEstate.setTotalFloors(realEstate.getTotalFloors());
        existingRealEstate.setNumberFloors(realEstate.getNumberFloors());
        existingRealEstate.setBath(realEstate.getBath());
        existingRealEstate.setApartCode(realEstate.getApartCode());
        existingRealEstate.setWallArea(realEstate.getWallArea());
        existingRealEstate.setBedroom(realEstate.getBedroom());
        existingRealEstate.setBalcony(realEstate.getBalcony());
        existingRealEstate.setLandscapeView(realEstate.getLandscapeView());
        existingRealEstate.setApartLoca(realEstate.getApartLoca());
        existingRealEstate.setApartType(realEstate.getApartType());
        existingRealEstate.setFurnitureType(realEstate.getFurnitureType());
        existingRealEstate.setPriceRent(realEstate.getPriceRent());
        existingRealEstate.setReturnRate(realEstate.getReturnRate());
        existingRealEstate.setLegalDoc(realEstate.getLegalDoc());
        existingRealEstate.setDescription(realEstate.getDescription());
        existingRealEstate.setWidthY(realEstate.getWidthY());
        existingRealEstate.setLongX(realEstate.getLongX());
        existingRealEstate.setStreetHouse(realEstate.getStreetHouse());
        existingRealEstate.setFsbo(realEstate.getFsbo());
        existingRealEstate.setViewNum(realEstate.getViewNum());
        existingRealEstate.setCreateBy(realEstate.getCreateBy());
        existingRealEstate.setUpdateBy(realEstate.getUpdateBy());
        existingRealEstate.setShape(realEstate.getShape());
        existingRealEstate.setDistance2facade(realEstate.getDistance2facade());
        existingRealEstate.setAdjacentFacadeNum(realEstate.getAdjacentFacadeNum());
        existingRealEstate.setAdjacentRoad(realEstate.getAdjacentRoad());
        existingRealEstate.setAlleyMinWidth(realEstate.getAlleyMinWidth());
        existingRealEstate.setAdjacentAlleyMinWidth(realEstate.getAdjacentAlleyMinWidth());
        existingRealEstate.setFactor(realEstate.getFactor());
        existingRealEstate.setStructure(realEstate.getStructure());
        existingRealEstate.setDtsxd(realEstate.getDtsxd());
        existingRealEstate.setClcl(realEstate.getClcl());
        existingRealEstate.setCtxdPrice(realEstate.getCtxdPrice());
        existingRealEstate.setCtxdValue(realEstate.getCtxdValue());
        existingRealEstate.setPhoto(realEstate.getPhoto());
        existingRealEstate.setLat(realEstate.getLat());
        existingRealEstate.setLng(realEstate.getLng());

        return realEstateRepository.save(existingRealEstate);
    }

    // Delete a real estate
    public void deleteRealEstate(int id) {
        realEstateRepository.deleteById(id);
    }
}
