package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.District;
import com.devcamp.capstonehome24h.Entity.Ward;
import com.devcamp.capstonehome24h.Repository.DistrictRepository;
import com.devcamp.capstonehome24h.Repository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository iWard;

    @Autowired
    DistrictRepository iDistrict;

    // Hàm lấy ra toàn bộ ward
    public List<Ward> getAll() {
        return iWard.findAll();
    }

    // Hàm tạo mới 1 ward
    public Ward createWard(Ward pWard, Integer districtId) {
        District vDistrict = iDistrict.findById(districtId).get();
        pWard.setDistrict(vDistrict);
        Ward wardCreated = iWard.save(pWard);
        return wardCreated;
    }

    // Hàm cập nhật 1 ward
    public Ward updateWard(Ward pWard, Integer wardId, Integer districtId) {
        Ward vWard = iWard.findById(wardId).get();
        District vDistrict = iDistrict.findById(districtId).get();
        vWard.setDistrict(vDistrict);
        vWard.setName(pWard.getName());
        vWard.setPrefix(pWard.getPrefix());
        Ward updatedWard = iWard.save(vWard);
        return updatedWard;
    }

    // Hàm xóa 1 ward
    public void deleteWard(Integer wardId){
        iWard.deleteById(wardId);
    }
}
