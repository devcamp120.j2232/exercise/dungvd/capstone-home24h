package com.devcamp.capstonehome24h.Service;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Customer;
import com.devcamp.capstonehome24h.Repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository iCustomer;

    // Hàm lấy toàn bộ thông tin customer
    public List<Customer> getAll() {
        return iCustomer.findAll();
    }

    // Hàm tạo mới 1 customer
    public Customer createCustomer(Customer pCustomer) {
        Date vDate = new Date();
        pCustomer.setCreateDate(vDate);
        Customer customerCreated = iCustomer.save(pCustomer);
        return customerCreated;
    }

    // Hàm sửa 1 customer
    public Customer updateCustomer(Customer pCustomer, Integer id) {
        Date vDate = new Date();
        Customer vCustomer = iCustomer.findById(id).get();
        vCustomer.setUpdateDate(vDate);
        vCustomer.setAddress(pCustomer.getAddress());
        vCustomer.setContactName(pCustomer.getContactName());
        vCustomer.setContactTitle(pCustomer.getContactTitle());
        vCustomer.setEmail(pCustomer.getEmail());
        vCustomer.setNote(pCustomer.getNote());
        vCustomer.setMobile(pCustomer.getMobile());
        Customer updatedCustomer = iCustomer.save(vCustomer);
        return updatedCustomer;
    }

    // Hàm xóa 1 customer
    public void deleteCustomer(Integer id) {
        iCustomer.deleteById(id);
    }
}
