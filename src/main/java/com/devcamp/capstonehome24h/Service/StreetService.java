package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.District;
import com.devcamp.capstonehome24h.Entity.Street;
import com.devcamp.capstonehome24h.Repository.DistrictRepository;
import com.devcamp.capstonehome24h.Repository.StreetRepository;
 
@Service
public class StreetService {
    @Autowired
    StreetRepository iStreet;

    @Autowired
    DistrictRepository iDistrict;

    // Hàm lấy ra toàn bộ street
    public List<Street> getAll() {
        return iStreet.findAll();
    }

    // Hàm tạo mới 1 street
    public Street createStreet(Street pStreet, Integer districtId) {
        District vDistrict = iDistrict.findById(districtId).get();
        pStreet.setDistrict(vDistrict);
        Street streetCreated = iStreet.save(pStreet);
        return streetCreated;
    }

    // Hàm cập nhật 1 street
    public Street updateStreet(Street pStreet, Integer streetId, Integer districtId) {
        Street vStreet = iStreet.findById(streetId).get();
        District vDistrict = iDistrict.findById(districtId).get();
        vStreet.setDistrict(vDistrict);
        vStreet.setName(pStreet.getName());
        vStreet.setPrefix(pStreet.getPrefix());
        Street updatedStreet = iStreet.save(vStreet);
        return updatedStreet;
    }

    // Hàm xóa 1 ward
    public void deleteStreet(Integer streetId){
        iStreet.deleteById(streetId);
    }
}
