package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Province;
import com.devcamp.capstonehome24h.Repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository iProvince;
    
    // Hàm lấy ra toàn bộ province
    public List<Province> getAll() {
        return iProvince.findAll();
    }

    // Hàm tạo mới 1 province
    public Province createProvince(Province pProvince) {
        Province provinceCreated = iProvince.save(pProvince);
        return provinceCreated;
    }

    // Hàm cập nhật 1 province
    public Province updateProvince(Province pProvince, Integer provinceId) {
        Province vProvince = iProvince.findById(provinceId).get();
        vProvince.setName(pProvince.getName());
        vProvince.setCode(pProvince.getCode());
        Province updatedProvince = iProvince.save(vProvince);
        return updatedProvince;
    }

    // Hàm xóa 1 province
    public void deleteProvince(Integer provinceId){
        iProvince.deleteById(provinceId);
    }
}
