package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Utilities;
import com.devcamp.capstonehome24h.Repository.UtilitiesRepository;

@Service
public class UtilitesService {
    @Autowired
    UtilitiesRepository iUtilities;

    // lấy ra toàn bộ uti
    public List<Utilities> getAll() {
        return iUtilities.findAll();
    }

    // Tạo mới 1 utiti
    public Utilities createUtilities(Utilities pUtilities) {
        Utilities createdUtilities = iUtilities.save(pUtilities);
        return createdUtilities;
    }

    // Cập nhật 1 utili
    public Utilities updateUtilites(Utilities pUtilities, int id) {
        Utilities vUtilities = iUtilities.findById(id).get();
        vUtilities.setDescription(pUtilities.getDescription());
        vUtilities.setName(pUtilities.getName());
        vUtilities.setPhoto(pUtilities.getPhoto());
        Utilities updatedUtilities = iUtilities.save(vUtilities);
        return updatedUtilities;
    }

    // xóa 1 utili
    public void deleteUtilities(int id) {
        iUtilities.deleteById(id);
    }
}
