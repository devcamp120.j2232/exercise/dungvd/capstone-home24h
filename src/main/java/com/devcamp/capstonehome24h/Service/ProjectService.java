package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Project;
import com.devcamp.capstonehome24h.Repository.ProjectRepository;

@Service
public class ProjectService {
    

    @Autowired
    ProjectRepository projectRepository;
   
    // Get all projects
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    // Create a new project
    public Project createProject(Project project) {
        return projectRepository.save(project);
    }

    // Update a project
    public Project updateProject(Project project, int id) {
        Project existingProject = projectRepository.findById(id).get();
        existingProject.setName(project.getName());
        existingProject.setProvinceId(project.getProvinceId());
        existingProject.setDistrictId(project.getDistrictId());
        existingProject.setWardId(project.getWardId());
        existingProject.setStreetId(project.getStreetId());
        existingProject.setAddress(project.getAddress());
        existingProject.setSlogan(project.getSlogan());
        existingProject.setDescription(project.getDescription());
        existingProject.setAcreage(project.getAcreage());
        existingProject.setConstructArea(project.getConstructArea());
        existingProject.setNumBlock(project.getNumBlock());
        existingProject.setNumFloors(project.getNumFloors());
        existingProject.setNumApartment(project.getNumApartment());
        existingProject.setApartmentArea(project.getApartmentArea());
        existingProject.setInvestor(project.getInvestor());
        existingProject.setConstructionContractor(project.getConstructionContractor());
        existingProject.setDesignUnit(project.getDesignUnit());
        existingProject.setUtilities(project.getUtilities());
        existingProject.setRegionLink(project.getRegionLink());
        existingProject.setPhoto(project.getPhoto());
        existingProject.setLatitude(project.getLatitude());
        existingProject.setLongitude(project.getLongitude());
        return projectRepository.save(existingProject);
    }

    // Delete a project
    public void deleteProject(int id) {
        projectRepository.deleteById(id);
    }
}
