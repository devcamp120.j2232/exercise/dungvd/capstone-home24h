package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.ConstructionContractor;
import com.devcamp.capstonehome24h.Repository.ConstructionContractorRepository;


@Service
public class ConstructionContractorService {
    @Autowired
    ConstructionContractorRepository iConstructionContractor;



    // Hàm lấy ra toàn bộ Construction Contractor
    public List<ConstructionContractor> getAll() {
        return iConstructionContractor.findAll();
    }

    // Hàm tạo mới 1 construction contractor
    public ConstructionContractor createConstructionContractor(ConstructionContractor pConstructionContractor) {
        ConstructionContractor createdConstructionContractor = iConstructionContractor.save(pConstructionContractor);
        return createdConstructionContractor;
    }

    // Hàm sửa 1 construction contractor
    public ConstructionContractor updateConstructionContractor(ConstructionContractor pConstructionContractor, Integer id) {
        ConstructionContractor vConstructionContractor = iConstructionContractor.findById(id).get();
        vConstructionContractor.setAddress(pConstructionContractor.getAddress()); 
        vConstructionContractor.setDescription(pConstructionContractor.getDescription()); 
        vConstructionContractor.setEmail(pConstructionContractor.getEmail()); 
        vConstructionContractor.setName(pConstructionContractor.getName()); 
        vConstructionContractor.setPhone(pConstructionContractor.getPhone()); 
        vConstructionContractor.setPhone2(pConstructionContractor.getPhone2()); 
        vConstructionContractor.setNote(pConstructionContractor.getNote()); 
        vConstructionContractor.setProjects(pConstructionContractor.getProjects()); 
        vConstructionContractor.setWebsite(pConstructionContractor.getWebsite()); 
        vConstructionContractor.setFax(pConstructionContractor.getFax()); 
        ConstructionContractor vCCUpdated = iConstructionContractor.save(vConstructionContractor);
        return vCCUpdated;
    }

    // Hàm xóa 1 construction contractor
    public void deleteConstructionContractor(Integer id) {
        iConstructionContractor.deleteById(id);
    }
}
