package com.devcamp.capstonehome24h.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.AddressMap;
import com.devcamp.capstonehome24h.Repository.AddressMapRepository;

@Service
public class AddressMapService {
    @Autowired
    AddressMapRepository iAddressMap;

    // Lấy thông tin toàn bộ Address Map
    public List<AddressMap> getAllAddressMap() {
        List<AddressMap> allAddressMap = new ArrayList<>();
        iAddressMap.findAll().forEach(allAddressMap::add);
        return allAddressMap;
    }

    // Thêm mới 1 Address Map
    public AddressMap createAddressMap(AddressMap pAddressMap) {
        AddressMap addressMapCreated = iAddressMap.save(pAddressMap);
        return addressMapCreated;
    } 

    // Sửa 1 Address map
    public AddressMap updateAddressMap(AddressMap pAddressMap, Integer id) {
        AddressMap vAddressMap = iAddressMap.findById(id).get();
        vAddressMap.setAddress(pAddressMap.getAddress());
        vAddressMap.setLatitude(pAddressMap.getLatitude());
        vAddressMap.setLongitude(pAddressMap.getLongitude());
        AddressMap vAddressMapUpdated = iAddressMap.save(vAddressMap);
        return vAddressMapUpdated;
    }


    // Xóa 1 Address map
    public void deleteAddressMap(Integer id) {
        iAddressMap.deleteById(id);
    }
}
