package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.RegionLink;
import com.devcamp.capstonehome24h.Repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    RegionLinkRepository iRegionLink;

    // get all region Link
    public List<RegionLink> getAll() {
        return iRegionLink.getAllRegionLinks();
    }

    // create region link
    public RegionLink createRegionLink(RegionLink pRegionLink) {
        RegionLink createdRegionLink = iRegionLink.save(pRegionLink);
        return createdRegionLink;
    }

    // update region link
    public RegionLink updateRegionLink(RegionLink pRegionLink, int id) {
        RegionLink vRegionLink = iRegionLink.findById(id).get();
        vRegionLink.setAddress(pRegionLink.getAddress());
        vRegionLink.setDescription(pRegionLink.getDescription());
        vRegionLink.setLatitude(pRegionLink.getLatitude());
        vRegionLink.setLongitude(pRegionLink.getLongitude());
        vRegionLink.setName(pRegionLink.getName());
        vRegionLink.setPhoto(pRegionLink.getPhoto());
        RegionLink regionLinkUpdated = iRegionLink.save(vRegionLink);
        return regionLinkUpdated;
    }

    // Delete by id
    public void deleteRegionLink(int id) {
        iRegionLink.deleteById(id);
    }
}
