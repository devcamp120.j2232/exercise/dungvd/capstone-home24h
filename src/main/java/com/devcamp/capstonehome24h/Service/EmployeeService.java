package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Employee;
import com.devcamp.capstonehome24h.Repository.EmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository iEmployee;

    // Hàm lấy ra toàn bộ employee
    public List<Employee> getAll() {
        return iEmployee.findAll();
    }

    // Hàm tạo mới 1 employee
    public Employee createEmployee(Employee pEmployee) {
        Employee employeeCreated = iEmployee.save(pEmployee);
        return employeeCreated;
    }

    // Hàm cập nhật 1 employee
    public Employee updateEmployee(Employee pEmployee, Integer employeeId) {
        Employee vEmployee = iEmployee.findById(employeeId).get();
        vEmployee.setActivated(pEmployee.isActivated());
        vEmployee.setAddress(pEmployee.getAddress());
        vEmployee.setBirthDate(pEmployee.getBirthDate());
        vEmployee.setCity(pEmployee.getCity());
        vEmployee.setCountry(pEmployee.getCountry());
        vEmployee.setEmail(pEmployee.getEmail());
        vEmployee.setExtension(pEmployee.getExtension());
        vEmployee.setFirstName(pEmployee.getFirstName());
        vEmployee.setHireDate(pEmployee.getHireDate());
        vEmployee.setHomePhone(pEmployee.getHomePhone());
        vEmployee.setLastName(pEmployee.getLastName());
        vEmployee.setNotes(pEmployee.getNotes());
        vEmployee.setPassword(pEmployee.getPassword());
        vEmployee.setPhoto(pEmployee.getPhoto());
        vEmployee.setPostalCode(pEmployee.getPostalCode());
        vEmployee.setProfile(pEmployee.getProfile());
        vEmployee.setRegion(pEmployee.getRegion());
        vEmployee.setTitle(pEmployee.getTitle());
        vEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
        vEmployee.setUserLevel(pEmployee.getUserLevel());
        vEmployee.setUsername(pEmployee.getUsername());
        Employee updatedEmployee = iEmployee.save(vEmployee);
        return updatedEmployee;
    }

    // Hàm xóa 1 employee
    public void deleteEmployee(Integer employeeId){
        iEmployee.deleteById(employeeId);
    }
}
