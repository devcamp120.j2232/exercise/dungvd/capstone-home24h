package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.DesignUnit;
import com.devcamp.capstonehome24h.Repository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository iDesignUnit;

    // Lấy ra toàn bộ đơn vị thiết kế
    public List<DesignUnit> getAll() {
        return iDesignUnit.findAll();
    }

    // Hàm tạo mới 1 đơn vị thiết kế
    public DesignUnit createDesignUnit(DesignUnit pDesignUnit) {
        DesignUnit createDesignUnit = iDesignUnit.save(pDesignUnit);
        return createDesignUnit;
    }

    // Hàm update 1 design unit
    public DesignUnit updateDesignUnit(DesignUnit pDesignUnit, Integer id) {
        DesignUnit vDesignUnit = iDesignUnit.findById(id).get();
        vDesignUnit.setAddress(pDesignUnit.getAddress()); 
        vDesignUnit.setDescription(pDesignUnit.getDescription()); 
        vDesignUnit.setEmail(pDesignUnit.getEmail()); 
        vDesignUnit.setName(pDesignUnit.getName()); 
        vDesignUnit.setPhone(pDesignUnit.getPhone()); 
        vDesignUnit.setPhone2(pDesignUnit.getPhone2()); 
        vDesignUnit.setNote(pDesignUnit.getNote()); 
        vDesignUnit.setProjects(pDesignUnit.getProjects()); 
        vDesignUnit.setWebsite(pDesignUnit.getWebsite()); 
        vDesignUnit.setFax(pDesignUnit.getFax()); 
        DesignUnit updateDesignUnit = iDesignUnit.save(vDesignUnit);
        return updateDesignUnit;
    }

    // Hàm xóa 1 design unit
    public void deleteDesignUnit(Integer id) {
        iDesignUnit.deleteById(id);
    }
}
