package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Employee;
import com.devcamp.capstonehome24h.Entity.Subscription;
import com.devcamp.capstonehome24h.Repository.EmployeeRepository;
import com.devcamp.capstonehome24h.Repository.SubcriptionRepository;

@Service
public class SubcriptionService {
    @Autowired
    SubcriptionRepository subscriptionRepository;

    @Autowired 
    EmployeeRepository iEmployee;

    // Get all subscriptions
    public List<Subscription> getAll() {
        return subscriptionRepository.findAll();
    }

    // Create a new subscription
    public Subscription createSubscription(Subscription subscription, int userId) {
        Employee vEmployee = iEmployee.findById(userId).get();
        subscription.setEmployee(vEmployee);
        return subscriptionRepository.save(subscription);
    }

    // Update a subscription
    public Subscription updateSubscription(Subscription subscription, int id, int userId) {
        Employee vEmployee = iEmployee.findById(userId).get();
        Subscription existingSubscription = subscriptionRepository.findById(id).get();
            existingSubscription.setEmployee(vEmployee);
            existingSubscription.setEndpoint(subscription.getEndpoint());
            existingSubscription.setPublicKey(subscription.getPublicKey());
            existingSubscription.setAuthenticationToken(subscription.getAuthenticationToken());
            existingSubscription.setContentEncoding(subscription.getContentEncoding());
            return subscriptionRepository.save(existingSubscription);
    }

    // Delete a subscription
    public void deleteSubscription(int id) {
        subscriptionRepository.deleteById(id);
    }
}
