package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.Investor;
import com.devcamp.capstonehome24h.Repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository iInvestor;

    // Lấy ra tất cả investor
    public List<Investor> getAll() {
        return iInvestor.findAll();
    }

    // Tạo mới 1 investor
    public Investor createInvestor(Investor pInvestor) {
        Investor investorCreated = iInvestor.save(pInvestor);
        return investorCreated;
    }

    // update 1 investor
    public Investor updateInvestor(Investor pInvestor,  int id) {
        Investor vInvestor = iInvestor.findById(id).get();
        vInvestor.setAddress(pInvestor.getAddress());
        vInvestor.setDescription(pInvestor.getDescription());
        vInvestor.setEmail(pInvestor.getEmail());
        vInvestor.setFax(pInvestor.getFax());
        vInvestor.setNote(pInvestor.getNote());
        vInvestor.setName(pInvestor.getName());
        vInvestor.setPhone(pInvestor.getPhone());
        vInvestor.setPhone2(pInvestor.getPhone2());
        vInvestor.setProjects(pInvestor.getProjects());
        vInvestor.setWebsite(pInvestor.getWebsite());
        Investor investorUpdated = iInvestor.save(vInvestor);
        return investorUpdated;
    }

    // delete investor
    public void deleteInvestor(int id) {
        iInvestor.deleteById(id);
    }
}
