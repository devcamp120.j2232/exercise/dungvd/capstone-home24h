package com.devcamp.capstonehome24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.capstonehome24h.Entity.District;
import com.devcamp.capstonehome24h.Entity.Province;
import com.devcamp.capstonehome24h.Repository.DistrictRepository;
import com.devcamp.capstonehome24h.Repository.ProvinceRepository;

@Service
public class DistrictSerivce {
    @Autowired
    DistrictRepository iDistrict;

    @Autowired
    ProvinceRepository iProvince;
    
    // Hàm lấy ra toàn bộ district
    public List<District> getAll() {
        return iDistrict.findAll();
    }

    // Hàm tạo mới 1 district
    public District createDistrict(District pDistrict, Integer provinceId) {
        Province vProvince = iProvince.findById(provinceId).get();
        pDistrict.setProvince(vProvince);
        District districtCreated = iDistrict.save(pDistrict);
        return districtCreated;
    }

    // Hàm cập nhật 1 district
    public District updateDistrict(District pDistrict, Integer provinceId, Integer districtId) {
        Province vProvince = iProvince.findById(provinceId).get();
        District vDistrict = iDistrict.findById(districtId).get();
        vDistrict.setProvince(vProvince);
        vDistrict.setName(pDistrict.getName());
        vDistrict.setPrefix(pDistrict.getPrefix());
        District updatedDistrict = iDistrict.save(vDistrict);
        return updatedDistrict;
    }

    // Hàm xóa 1 district
    public void deleteDistrict(Integer districtId){
        iDistrict.deleteById(districtId);
    }
}
