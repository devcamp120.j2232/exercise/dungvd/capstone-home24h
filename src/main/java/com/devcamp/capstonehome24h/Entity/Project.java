package com.devcamp.capstonehome24h.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_province_id")
    private int provinceId;

    @Column(name = "_district_id")
    private int districtId;

    @Column(name = "_ward_id")
    private int wardId;

    @Column(name = "_street_id")
    private int streetId;

    @Column
    private String address;

    @Column
    private String slogan;

    @Column
    private String description;

    @Column
    private double acreage;

    @Column(name = "construct_area")
    private double constructArea;

    @Column
    private int numBlock;

    @Column
    private String numFloors;

    @Column
    private int numApartment;

    @Column(name = "apartment_area")
    private String apartmentArea;

    @Column
    private int investor;

    @Column(name = "construction_contractor")
    private int constructionContractor;

    @Column(name = "design_unit")
    private int designUnit;

    @Column
    private String utilities;

    @Column(name = "region_link")
    private String regionLink;

    @Column
    private String photo;

    @Column(name = "_lat")
    private double latitude;

    @Column(name = "_lng")
    private double longitude;

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAcreage() {
        return acreage;
    }

    public void setAcreage(double acreage) {
        this.acreage = acreage;
    }

    public double getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(double constructArea) {
        this.constructArea = constructArea;
    }

    public int getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(int numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public int getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(int numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public int getInvestor() {
        return investor;
    }

    public void setInvestor(int investor) {
        this.investor = investor;
    }

    public int getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(int constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public int getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(int designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    
}
