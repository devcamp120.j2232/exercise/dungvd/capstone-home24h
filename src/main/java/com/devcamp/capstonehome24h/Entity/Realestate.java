package com.devcamp.capstonehome24h.Entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "realestate")
public class Realestate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type")
    private int type;

    @Column(name = "request")
    private int request;

    @Column(name = "province_id")
    private int provinceId;

    @Column(name = "district_id")
    private int districtId;

    @Column(name = "wards_id")
    private int wardsId;

    @Column(name = "street_id")
    private int streetId;

    @Column(name = "project_id")
    private int projectId;

    @Column(name = "address", length = 2000)
    private String address;

    @Column(name = "customer_id")
    private int customerId;

    @Column(name = "price")
    private long price;

    @Column(name = "price_min")
    private long priceMin;

    @Column(name = "price_time")
    private int priceTime;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "acreage")
    private BigDecimal acreage;

    @Column(name = "direction")
    private int direction;

    @Column(name = "total_floors")
    private int totalFloors;

    @Column(name = "number_floors")
    private int numberFloors;

    @Column(name = "bath")
    private int bath;

    @Column(name = "apart_code", length = 10)
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    @Column(name = "bedroom")
    private int bedroom;

    @Column(name = "balcony")
    private int balcony;

    @Column(name = "landscape_view", length = 255)
    private String landscapeView;

    @Column(name = "apart_loca")
    private int apartLoca;

    @Column(name = "apart_type")
    private int apartType;

    @Column(name = "furniture_type")
    private int furnitureType;

    @Column(name = "price_rent")
    private int priceRent;

    @Column(name = "return_rate")
    private double returnRate;

    @Column(name = "legal_doc")
    private int legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private int widthY;

    @Column(name = "long_x")
    private int longX;

    @Column(name = "street_house")
    private int streetHouse;

    @Column(name = "FSBO")
    private int fsbo;

    @Column(name = "view_num")
    private int viewNum;

    @Column(name = "create_by")
    private int createBy;

    @Column(name = "update_by")
    private int updateBy;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade")
    private int distance2facade;

    @Column(name = "adjacent_facade_num")
    private int adjacentFacadeNum;

    @Column(name = "adjacent_road", length = 200)
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private int alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private int adjacentAlleyMinWidth;

    @Column(name = "factor")
    private int factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "DTSXD")
    private int dtsxd;

    @Column(name = "CLCL")
    private int clcl;

    @Column(name = "CTXD_price")
    private int ctxdPrice;

    @Column(name = "CTXD_value")
    private int ctxdValue;

    @Column(name = "photo", length = 2000)
    private String photo;

    @Column(name = "_lat")
    private double lat;

    @Column(name = "_lng")
    private double lng;

    public Realestate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getWardsId() {
        return wardsId;
    }

    public void setWardsId(int wardsId) {
        this.wardsId = wardsId;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(long priceMin) {
        this.priceMin = priceMin;
    }

    public int getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(int priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(int totalFloors) {
        this.totalFloors = totalFloors;
    }

    public int getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(int numberFloors) {
        this.numberFloors = numberFloors;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBalcony() {
        return balcony;
    }

    public void setBalcony(int balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public int getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(int apartLoca) {
        this.apartLoca = apartLoca;
    }

    public int getApartType() {
        return apartType;
    }

    public void setApartType(int apartType) {
        this.apartType = apartType;
    }

    public int getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(int furnitureType) {
        this.furnitureType = furnitureType;
    }

    public int getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(int priceRent) {
        this.priceRent = priceRent;
    }

    public double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(double returnRate) {
        this.returnRate = returnRate;
    }

    public int getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(int legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidthY() {
        return widthY;
    }

    public void setWidthY(int widthY) {
        this.widthY = widthY;
    }

    public int getLongX() {
        return longX;
    }

    public void setLongX(int longX) {
        this.longX = longX;
    }

    public int getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(int streetHouse) {
        this.streetHouse = streetHouse;
    }

    public int getFsbo() {
        return fsbo;
    }

    public void setFsbo(int fsbo) {
        this.fsbo = fsbo;
    }

    public int getViewNum() {
        return viewNum;
    }

    public void setViewNum(int viewNum) {
        this.viewNum = viewNum;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(int distance2facade) {
        this.distance2facade = distance2facade;
    }

    public int getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(int adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public int getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(int alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public int getAdjacentAlleyMinWidth() {
        if (adjacentAlleyMinWidth > 0) {
            return this.adjacentAlleyMinWidth;
        } else {
            return 0;
        }
    }

    public void setAdjacentAlleyMinWidth(int adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public int getDtsxd() {
        return dtsxd;
    }

    public void setDtsxd(int dtsxd) {
        this.dtsxd = dtsxd;
    }

    public int getClcl() {
        return clcl;
    }

    public void setClcl(int clcl) {
        this.clcl = clcl;
    }

    public int getCtxdPrice() {
        return ctxdPrice;
    }

    public void setCtxdPrice(int ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public int getCtxdValue() {
        return ctxdValue;
    }

    public void setCtxdValue(int ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    
}
