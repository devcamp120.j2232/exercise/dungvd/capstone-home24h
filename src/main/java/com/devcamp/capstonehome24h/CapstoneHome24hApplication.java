package com.devcamp.capstonehome24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapstoneHome24hApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapstoneHome24hApplication.class, args);
	}

}
