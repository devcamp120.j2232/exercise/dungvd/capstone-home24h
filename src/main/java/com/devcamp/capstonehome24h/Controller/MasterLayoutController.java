package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.MasterLayout;
import com.devcamp.capstonehome24h.Repository.MasterLayoutRepository;
import com.devcamp.capstonehome24h.Service.MasterLayoutService;

@RestController
@CrossOrigin
public class MasterLayoutController {
    @Autowired
    MasterLayoutService masterLayoutService;

    @Autowired
    MasterLayoutRepository iMasterLayout;

    @GetMapping("/master-layouts")
    public ResponseEntity<List<MasterLayout>> findAll() {
        try {
            List<MasterLayout> masterLayouts = masterLayoutService.getAll();
            return new ResponseEntity<>(masterLayouts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get master layout by id
    @GetMapping("/master-layouts/{id}")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iMasterLayout.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/projects/{projectId}/master-layouts")
    public ResponseEntity<MasterLayout> createMasterLayout(@RequestBody MasterLayout masterLayout, @PathVariable("projectId") int projectId) {
        try {
            MasterLayout createdMasterLayout = masterLayoutService.createMasterLayout(masterLayout, projectId);
            return new ResponseEntity<>(createdMasterLayout, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/projects/{projectId}/master-layouts/{id}")
    public ResponseEntity<MasterLayout> updateMasterLayout(@RequestBody MasterLayout masterLayout, @PathVariable("id") int id, @PathVariable("projectId") int projectId) {
        try {
            MasterLayout updatedMasterLayout = masterLayoutService.updateMasterLayout(masterLayout, id, projectId);
            return new ResponseEntity<>(updatedMasterLayout, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/master-layouts/{id}")
    public ResponseEntity<MasterLayout> deleteMasterLayout(@PathVariable("id") int id) {
        try {
            masterLayoutService.deleteMasterLayout(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

