package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.RegionLink;
import com.devcamp.capstonehome24h.Repository.RegionLinkRepository;
import com.devcamp.capstonehome24h.Service.RegionLinkService;

@RestController
@CrossOrigin
public class RegionLinkController {
    @Autowired
    private RegionLinkService regionLinkService;

    @Autowired RegionLinkRepository iRegionLink;

    @GetMapping("/regionlinks")
    public ResponseEntity<List<RegionLink>> getAllRegionLinks() {
        try {
            List<RegionLink> regionLinks = regionLinkService.getAll();
            return new ResponseEntity<>(regionLinks, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get region link by idư
    @GetMapping("/regionlinks/{id}")
    public ResponseEntity<RegionLink> findRegionLinkById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iRegionLink.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/regionlinks")
    public ResponseEntity<RegionLink> createRegionLink(@RequestBody RegionLink regionLink) {
        try {
            RegionLink createdRegionLink = regionLinkService.createRegionLink(regionLink);
            return new ResponseEntity<>(createdRegionLink, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PutMapping("/regionlinks/{id}")
    public ResponseEntity<RegionLink> updateRegionLink(@PathVariable("id") int id, @RequestBody RegionLink regionLink) {
        try {
            RegionLink updatedRegionLink = regionLinkService.updateRegionLink(regionLink, id);
            return new ResponseEntity<>(updatedRegionLink, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/regionlinks/{id}")
    public ResponseEntity<HttpStatus> deleteRegionLink(@PathVariable("id") int id) {
        try {
            regionLinkService.deleteRegionLink(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

