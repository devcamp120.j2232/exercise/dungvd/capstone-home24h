package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Province;
import com.devcamp.capstonehome24h.Repository.ProvinceRepository;
import com.devcamp.capstonehome24h.Service.ProvinceService;

@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    @Autowired
    ProvinceRepository iProvince;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> findAll() {
        try {
            List<Province> provinces = provinceService.getAll();
            return new ResponseEntity<>(provinces, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> findProvinceById(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(iProvince.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/provinces")
    public ResponseEntity<Province> createProvince(@RequestBody Province province) {
        try {
            Province createdProvince = provinceService.createProvince(province);
            return new ResponseEntity<>(createdProvince, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{id}")
    public ResponseEntity<Province> updateProvince(@RequestBody Province province, @PathVariable("id") int id) {
        try {
            Province updatedProvince = provinceService.updateProvince(province, id);
            return new ResponseEntity<>(updatedProvince, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/provinces/{id}")
    public ResponseEntity<Province> deleteProvince(@PathVariable("id") int id) {
        try {
            provinceService.deleteProvince(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

