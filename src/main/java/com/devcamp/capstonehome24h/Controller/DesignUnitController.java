package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.DesignUnit;
import com.devcamp.capstonehome24h.Repository.DesignUnitRepository;
import com.devcamp.capstonehome24h.Service.DesignUnitService;

@RestController
@CrossOrigin
public class DesignUnitController {
    @Autowired
    DesignUnitService designUnitService;

    @Autowired
    DesignUnitRepository iDesignUnit;

    @GetMapping("/designunits")
    public ResponseEntity<List<DesignUnit>> findAll() {
        try {
            List<DesignUnit> designUnits = designUnitService.getAll();
            return new ResponseEntity<>(designUnits, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Get design unit by id
    @GetMapping("/designunits/{id}")
    public ResponseEntity<DesignUnit> getDesginUnitById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDesignUnit.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/designunits")
    public ResponseEntity<DesignUnit> createDesignUnit(@RequestBody DesignUnit designUnit) {
        try {
            DesignUnit createdDesignUnit = designUnitService.createDesignUnit(designUnit);
            return new ResponseEntity<>(createdDesignUnit, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/designunits/{id}")
    public ResponseEntity<DesignUnit> updateDesignUnit(@RequestBody DesignUnit designUnit, @PathVariable("id") int id) {
        try {
            DesignUnit updatedDesignUnit = designUnitService.updateDesignUnit(designUnit, id);
            return new ResponseEntity<>(updatedDesignUnit, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/designunits/{id}")
    public ResponseEntity<DesignUnit> deleteDesignUnit(@PathVariable("id") int id) {
        try {
            designUnitService.deleteDesignUnit(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

