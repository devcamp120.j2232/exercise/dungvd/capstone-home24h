package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.District;
import com.devcamp.capstonehome24h.Repository.DistrictRepository;
import com.devcamp.capstonehome24h.Service.DistrictSerivce;

@RestController
@CrossOrigin
public class DistrictController {
    @Autowired
    DistrictSerivce districtService;

    @Autowired
    DistrictRepository iDistrict;

    @GetMapping("/districts")
    public ResponseEntity<List<District>> findAll() {
        try {
            List<District> districts = districtService.getAll();
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}/districts")
    public ResponseEntity<List<District>> getListDistrictsByProvinceId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDistrict.getListDistrictsByProvinceId(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm layas ra district by district id
    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDistrict.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/provinces/{provinceId}/districts")
    public ResponseEntity<District> createDistrict(@RequestBody District district, @PathVariable("provinceId") int provinceId) {
        try {
            District createdDistrict = districtService.createDistrict(district, provinceId);
            return new ResponseEntity<>(createdDistrict, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{provinceId}/districts/{districtId}")
    public ResponseEntity<District> updateDistrict(@RequestBody District district, @PathVariable("provinceId") int provinceId, @PathVariable("districtId") int districtId) {
        try {
            District updatedDistrict = districtService.updateDistrict(district, provinceId, districtId);
            return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/districts/{id}")
    public ResponseEntity<District> deleteDistrict(@PathVariable("id") int id) {
        try {
            districtService.deleteDistrict(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

