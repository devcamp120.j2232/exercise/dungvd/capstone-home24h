package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Investor;
import com.devcamp.capstonehome24h.Repository.InvestorRepository;
import com.devcamp.capstonehome24h.Service.InvestorService;

@RestController
@CrossOrigin
public class InvestorController {
    @Autowired
    InvestorService investorService;


    @Autowired
    InvestorRepository iInvestor;

    @GetMapping("/investors")
    public ResponseEntity<List<Investor>> findAll() {
        try {
            List<Investor> investors = investorService.getAll();
            return new ResponseEntity<>(investors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/{id}")
    public ResponseEntity<Investor> getInvestorById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iInvestor.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/investors")
    public ResponseEntity<Investor> createInvestor(@RequestBody Investor investor) {
        try {
            Investor createdInvestor = investorService.createInvestor(investor);
            return new ResponseEntity<>(createdInvestor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/investors/{id}")
    public ResponseEntity<Investor> updateInvestor(@RequestBody Investor investor, @PathVariable("id") int id) {
        try {
            Investor updatedInvestor = investorService.updateInvestor(investor, id);
            return new ResponseEntity<>(updatedInvestor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/investors/{id}")
    public ResponseEntity<Investor> deleteInvestor(@PathVariable("id") int id) {
        try {
            investorService.deleteInvestor(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

