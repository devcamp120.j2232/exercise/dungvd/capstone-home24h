package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.ConstructionContractor;
import com.devcamp.capstonehome24h.Repository.ConstructionContractorRepository;
import com.devcamp.capstonehome24h.Service.ConstructionContractorService;

@RestController
@CrossOrigin
public class ConstructionContractorController {
    @Autowired
    ConstructionContractorService ccService;

    @Autowired
    ConstructionContractorRepository iConstructionContractor;

    @GetMapping("/construction-contractors")
    public ResponseEntity<List<ConstructionContractor>> findAll() {
        try {
            List<ConstructionContractor> constructionContractors = ccService.getAll();
            return new ResponseEntity<>(constructionContractors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get Construction contractor by id
    @GetMapping("/construction-contractors/{id}")
    public ResponseEntity<ConstructionContractor> getConstructionContractorById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iConstructionContractor.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/construction-contractors")
    public ResponseEntity<ConstructionContractor> createConstructionContractor(@RequestBody ConstructionContractor constructionContractor) {
        try {
            ConstructionContractor createdConstructionContractor = ccService.createConstructionContractor(constructionContractor);
            return new ResponseEntity<>(createdConstructionContractor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/construction-contractors/{id}")
    public ResponseEntity<ConstructionContractor> updateConstructionContractor(@RequestBody ConstructionContractor constructionContractor, @PathVariable("id") int id) {
        try {
            ConstructionContractor updatedConstructionContractor = ccService.updateConstructionContractor(constructionContractor, id);
            return new ResponseEntity<>(updatedConstructionContractor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/construction-contractors/{id}")
    public ResponseEntity<ConstructionContractor> deleteConstructionContractor(@PathVariable("id") int id) {
        try {
            ccService.deleteConstructionContractor(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

