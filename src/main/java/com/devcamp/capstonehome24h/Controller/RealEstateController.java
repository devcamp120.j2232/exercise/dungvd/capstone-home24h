package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Realestate;
import com.devcamp.capstonehome24h.Service.RealestateService;

@RestController
@CrossOrigin
public class RealEstateController {
    @Autowired
    private RealestateService realEstateService;

    @GetMapping("/realestates")
    public ResponseEntity<List<Realestate>> getAllRealEstates() {
        try {
            List<Realestate> realEstates = realEstateService.getAll();
            return new ResponseEntity<>(realEstates, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates")
    public ResponseEntity<Realestate> createRealEstate(@RequestBody Realestate realEstate) {
        try {
            Realestate createdRealEstate = realEstateService.createRealEstate(realEstate);
            return new ResponseEntity<>(createdRealEstate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/realestates/{id}")
    public ResponseEntity<Realestate> updateRealEstate(@PathVariable("id") int id, @RequestBody Realestate realEstate) {
        try {
            Realestate updatedRealEstate = realEstateService.updateRealEstate(realEstate, id );
            return new ResponseEntity<>(updatedRealEstate, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/realestates/{id}")
    public ResponseEntity<HttpStatus> deleteRealEstate(@PathVariable("id") int id) {
        try {
            realEstateService.deleteRealEstate(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

