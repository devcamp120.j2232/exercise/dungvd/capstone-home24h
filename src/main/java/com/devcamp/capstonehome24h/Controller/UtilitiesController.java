package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Utilities;
import com.devcamp.capstonehome24h.Repository.UtilitiesRepository;
import com.devcamp.capstonehome24h.Service.UtilitesService;

@RestController
@CrossOrigin
public class UtilitiesController {
    @Autowired
    private UtilitesService utilitiesService;

    @Autowired
    UtilitiesRepository iUtilities;

    @GetMapping("/utilities")
    public ResponseEntity<List<Utilities>> getAllUtilities() {
        try {
            List<Utilities> utilities = utilitiesService.getAll();
            return new ResponseEntity<>(utilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get utilities by Id
    @GetMapping("/utilities/{id}")
    public ResponseEntity<Utilities> getUtilitiesById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iUtilities.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/utilities")
    public ResponseEntity<Utilities> createUtilities(@RequestBody Utilities utilities) {
        try {
            Utilities createdUtilities = utilitiesService.createUtilities(utilities);
            return new ResponseEntity<>(createdUtilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/utilities/{id}")
    public ResponseEntity<Utilities> updateUtilities(@PathVariable("id") int id, @RequestBody Utilities utilities) {
        try {
            Utilities updatedUtilities = utilitiesService.updateUtilites(utilities, id);
            if (updatedUtilities != null) {
                return new ResponseEntity<>(updatedUtilities, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/utilities/{id}")
    public ResponseEntity<HttpStatus> deleteUtilities(@PathVariable("id") int id) {
        try {
            utilitiesService.deleteUtilities(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
