package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.AddressMap;
import com.devcamp.capstonehome24h.Repository.AddressMapRepository;
import com.devcamp.capstonehome24h.Service.AddressMapService;

@RestController
@CrossOrigin
public class AddressMapController {
    @Autowired
    AddressMapService iAddressMapService;

    @Autowired
    AddressMapRepository iAddressMapRepository;
    @GetMapping("/addressmaps")
    public ResponseEntity<List<AddressMap>> findAll() {
        try {
            return new ResponseEntity<>(iAddressMapService.getAllAddressMap(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // Get Data addressmap By id
    @GetMapping("addressmaps/{id}")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iAddressMapRepository.findById(id).get() ,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addressmaps")
    public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap pAddressMap) {
        try {
            return new ResponseEntity<>(iAddressMapService.createAddressMap(pAddressMap), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/addressmaps/{id}")
    public ResponseEntity<AddressMap> updateAddressMap(@RequestBody AddressMap pAddressMap,@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iAddressMapService.updateAddressMap(pAddressMap, id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/addressmaps/{id}")
    public ResponseEntity<AddressMap> deleleAddressMap(@PathVariable("id") int id) {
        try {
            iAddressMapService.deleteAddressMap(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
