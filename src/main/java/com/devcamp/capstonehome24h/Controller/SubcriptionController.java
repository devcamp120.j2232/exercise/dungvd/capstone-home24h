package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Subscription;
import com.devcamp.capstonehome24h.Repository.SubcriptionRepository;
import com.devcamp.capstonehome24h.Service.SubcriptionService;

@RestController
@CrossOrigin
public class SubcriptionController {
    @Autowired
    private SubcriptionService subscriptionService;

    @Autowired
    SubcriptionRepository isSubcriptionRepository;

    @GetMapping("/subscriptions")
    public ResponseEntity<List<Subscription>> getAllSubscriptions() {
        try {
            List<Subscription> subscriptions = subscriptionService.getAll();
            return new ResponseEntity<>(subscriptions, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get Subscription BY Id
    @GetMapping("/subscriptions/{id}")
    public ResponseEntity<Subscription> getSubscriptionById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(isSubcriptionRepository.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("employees/{id}/subscriptions")
    public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription subscription, @PathVariable("id") int id) {
        try {
            Subscription createdSubscription = subscriptionService.createSubscription(subscription, id);
            return new ResponseEntity<>(createdSubscription, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("employees/{userId}/subscriptions/{id}")
    public ResponseEntity<Subscription> updateSubscription(@PathVariable("id") int id, @RequestBody Subscription subscription, @PathVariable("userId") int userId) {
        try {
            Subscription updatedSubscription = subscriptionService.updateSubscription(subscription, id, userId);
            if (updatedSubscription != null) {
                return new ResponseEntity<>(updatedSubscription, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/subscriptions/{id}")
    public ResponseEntity<HttpStatus> deleteSubscription(@PathVariable("id") int id) {
        try {
            subscriptionService.deleteSubscription(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

