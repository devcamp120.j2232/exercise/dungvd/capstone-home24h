package com.devcamp.capstonehome24h.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.capstonehome24h.Entity.Ward;
import com.devcamp.capstonehome24h.Repository.WardRepository;
import com.devcamp.capstonehome24h.Service.WardService;

@RestController
@CrossOrigin
public class WardController {
    @Autowired
    WardService wardService;

    @Autowired
    WardRepository iWard;

    @GetMapping("/wards")
    public ResponseEntity<List<Ward>> findAll() {
        try {
            List<Ward> wards = wardService.getAll();
            return new ResponseEntity<>(wards, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get all ward by district ID
    @GetMapping("districts/{id}/wards")
    public ResponseEntity<List<Ward>> getAllWardsByDistrictId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iWard.getAllWardByDistrictId(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET ward by ward id
    @GetMapping("wards/{id}")
    public ResponseEntity<Ward> getWardById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iWard.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
       
        }
    }

    @PostMapping("/districts/{districtId}/wards")
    public ResponseEntity<Ward> createWard(@RequestBody Ward ward, @PathVariable("districtId") int districtId) {
        try {
            Ward createdWard = wardService.createWard(ward, districtId);
            return new ResponseEntity<>(createdWard, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/districts/{districtId}/wards/{wardId}")
    public ResponseEntity<Ward> updateWard(@RequestBody Ward ward, @PathVariable("districtId") int districtId, @PathVariable("wardId") int wardId) {
        try {
            Ward updatedWard = wardService.updateWard(ward, wardId, districtId);
            return new ResponseEntity<>(updatedWard, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/wards/{id}")
    public ResponseEntity<Ward> deleteWard(@PathVariable("id") int id) {
        try {
            wardService.deleteWard(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

