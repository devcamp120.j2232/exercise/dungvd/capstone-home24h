package com.devcamp.capstonehome24h.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.capstonehome24h.Entity.RegionLink;

public interface RegionLinkRepository extends JpaRepository<RegionLink, Integer>{
    @Query(value = "SELECT * FROM `region_link`", nativeQuery = true)
    List<RegionLink> getAllRegionLinks();

}
