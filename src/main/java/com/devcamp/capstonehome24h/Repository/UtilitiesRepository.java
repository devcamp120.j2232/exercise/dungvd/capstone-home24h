package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Utilities;

public interface UtilitiesRepository extends JpaRepository<Utilities, Integer>{
    
}
