package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.AddressMap;

public interface AddressMapRepository extends JpaRepository<AddressMap, Integer>{
    
}
