package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
}
