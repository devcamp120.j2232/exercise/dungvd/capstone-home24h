package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.MasterLayout;

public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Integer>{
    
}
