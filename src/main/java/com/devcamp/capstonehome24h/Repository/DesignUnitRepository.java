package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer>{
    
}
