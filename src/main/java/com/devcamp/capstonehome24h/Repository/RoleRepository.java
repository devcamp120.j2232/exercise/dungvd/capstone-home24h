package com.devcamp.capstonehome24h.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.models.ERole;
import com.devcamp.capstonehome24h.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
