package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer>{
    
}
