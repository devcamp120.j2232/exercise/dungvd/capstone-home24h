package com.devcamp.capstonehome24h.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.capstonehome24h.Entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer>{
    @Query(value = "SELECT * FROM `district` WHERE `_province_id` =:provinceId", nativeQuery = true)
    List<District> getListDistrictsByProvinceId(@Param("provinceId") int provinceId);
}
