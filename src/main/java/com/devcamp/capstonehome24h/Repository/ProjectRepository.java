package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{
    
}
