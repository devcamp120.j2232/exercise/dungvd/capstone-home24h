package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Realestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer>{
    
}
