package com.devcamp.capstonehome24h.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.capstonehome24h.Entity.Street;

public interface StreetRepository extends JpaRepository<Street, Integer>{
    @Query(value = "SELECT * FROM `street` WHERE `_district_id` =:districtId", nativeQuery = true)
    List<Street> getAllWardByDistrictId(@Param("districtId") int districtId);
}
