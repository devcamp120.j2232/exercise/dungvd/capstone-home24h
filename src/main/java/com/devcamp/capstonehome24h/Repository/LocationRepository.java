package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer>{
    
}
