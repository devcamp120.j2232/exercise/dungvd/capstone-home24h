package com.devcamp.capstonehome24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.capstonehome24h.Entity.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer>{
    
}
